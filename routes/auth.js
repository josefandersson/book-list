const router = require('express').Router();

router.use((req, _, next) => {
	req.isLan = req.ipInfo.ip === '::1'
		|| req.ipInfo.ip === '::ffff:127.0.0.1'
		|| /(::ffff:)?192.168.1.[0-9]{1,3}/.test(req.ipInfo.ip);
	req.isKobo = req.headers['user-agent']?.indexOf('Kobo') > -1
		|| req.cookies?.overwriteType === 'kobo';
	req.isKindle = req.headers['user-agent']?.indexOf('Kindle') > -1
		|| req.cookies?.overwriteType === 'kindle';
	req.isEReader = req.isKobo
		|| req.isKindle;
	next();
});

router.use('/api', (req, _, next) => {
	req.canEdit = req.isLan;
	next();
});

router.use('/ul', (req, _, next) => {
	req.canUpload = req.isLan;
	next();
});

module.exports = router;
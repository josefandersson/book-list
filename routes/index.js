const router = require('express').Router();

const BookRepository = require('../src/BookRepository');
const FileRepository = require('../src/FileRepository');
const Util = require('../src/Util');

// ?hidden -> include hidden books
router.get('/', async (req, res) => {
	const limit = +req.query.limit;
	const page = +req.query.page || 1;
	const sort = req.query.sort || 'latest';

	const filters = {
		sort: Util.insideOrDefault(Util.BOOK_FILTERS_MAP[sort], Util.BOOK_FILTERS.sort),
		order: Util.insideOrDefault(req.query.order, Util.BOOK_FILTERS.order),
		limit: isFinite(limit) && 0 < limit && limit < 100
			? limit
			: !req.isEReader
				? 10
				: req.isKobo
					? 6
					: 5,
		offset: null,
	};
	filters.offset = filters.limit * (page - 1);

	const books = await BookRepository.getBooksByFilters(filters);

	await Promise.all(books.map(async book => {
		book.files = await FileRepository.getFilesForBook(book.id, req.isKobo, req.isKindle);
		book.categories = await BookRepository.getCategoriesForBook(book.id);
	}));

	const categories = await BookRepository.getCategoriesByFilter();

	const mutateQuery = (...pairs) => {
		const newQuery = new URLSearchParams(req.query);
		pairs.forEach(([key, val]) => newQuery[val ? 'set' : 'delete'](key, val));
		return `?${newQuery.toString()}`;
	};

	const links = {
		sort: Object.fromEntries(
			[['latest', 'desc'], ['title', 'asc'], ['author', 'asc'], ['published', 'desc']].map(([name, preferredOrder]) =>
				[name, mutateQuery(
					['page', null],
					['sort', name],
					['order', sort !== name
						? preferredOrder
						: req.query.order === preferredOrder
							? preferredOrder === 'desc' ? 'asc' : 'desc'
							: preferredOrder === 'desc' ? 'desc' : 'asc']
				)])),
		page: {
			previous: page > 1 ? mutateQuery(['page', page > 2 ? page - 1 : null]) : null,
			next: mutateQuery(['page', page + 1])
		}
	};

	res.render('index', {
		books: books ?? [],
		categories: categories ?? [],

		isEReader: req.isEReader,
		isKindle: req.isKindle,

		canEdit: req.canEdit,

		filters,
		links,

		currentPage: page,
		currentSort: sort,

		isEn: req.isLan
			? false
			: req.ipInfo.country == null || req.ipInfo.country !== 'SE'
	});
});

module.exports = router;

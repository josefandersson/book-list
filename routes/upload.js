const router = require('express').Router();
const path = require('path');
const fs = require('fs');
const util = require('util');
const md5File = require('md5-file');

const config = require('../config');
const BookRepository = require('../src/BookRepository');
const FileRepository = require('../src/FileRepository');
const Calibre = require('../src/Calibre');
const Util = require('../src/Util');

router.get('/', (req, res) =>
	res.render('upload', { maxSize: config.MAX_FILE_SIZE, canUpload: req.canUpload }));

router.post('/', async (req, res) => {
	if (!req.canUpload)
		return res.json({ error: 'client does not have permission to upload' });

	if (!req.files || !req.files.files)
		return res.json({ error: 'client sent no files' });

	const files = req.files.files.length ? req.files.files : [req.files.files];

	// Check that files are allowed, and move them to books directory
	const results = {};
	const toConvert = {};
	await Promise.all(files.map(async file => {
		if (config.ALLOWED_MIMES.indexOf(file.mimetype) === -1) {
			results[file.name] = { error: 'disallowed mimetype', mimetype: file.mimetype };
			return;
		}

		const md5Exists = await FileRepository.md5Exists(file.md5);
		if (md5Exists) {
			results[file.name] = { error: 'file already exists' };
			return;
		}

		const extension = path.extname(file.name).replace(/^\./, '');
		const name = file.name.replace(new RegExp(`\.${extension}$`), '');
		const newFilename = Util.nextFilename(config.BOOKS_DIR, encodeURIComponent(name), extension);
		const newPath = path.resolve(config.BOOKS_DIR, newFilename);
		const coverFilename = Util.nextFilename(config.BOOKS_DIR, name, 'png');

		await file.mv(newPath);

		const meta = await Calibre.getBookMeta(newPath, 'public/covers', coverFilename);
		if (!meta) {
			results[file.name] = { error: 'failed to extract metadata' };
			return;
		}

		const authorId = await BookRepository.insertAuthor({
			name: meta.authors
		});

		const bookId = await BookRepository.insertBook({
			authorId,
			title: meta.title,
			brief: meta.comments,
			publishedDate: meta.published,
			coverFilename: meta.coverFilename
		});

		const fileFormatId = await FileRepository.insertFileFormat({
			extension,
			mimetype: file.mimetype
		});

		const fileId = await FileRepository.insertFile({
			bookId,
			fileFormatId,
			name: newFilename,
			size: file.size,
			md5: file.md5,
			originalName: file.name,
			coverFilename: meta.coverFilename,
			metadata: JSON.stringify(meta),
			blob: fs.readFileSync(newPath),
			cover_blob: meta.coverFilename
				? fs.readFileSync(path.resolve('public/covers', coverFilename))
				: null
		});

		if (!fileId) {
			results[file.name] = { error: 'failed to get a file id' };
			return;
		}

		config.AUTO_CONVERT_MIMES
			.filter(([fromMimetype, _]) => fromMimetype === file.mimetype)
			.map(([_, toMimetype]) => {
				if (!toConvert[file.name])
					toConvert[file.name] = [{ fileId, toMimetype }];
				else
					toConvert[file.name].push({ fileId, toMimetype });
			});

		results[file.name] = { authorId, bookId, fileFormatId, fileId };
	}));

	res.json({ results, toConvert });

	console.log('Converting...', toConvert);


	// TODO: Auto convert to missing formats (after ALL books/formats have been uploaded and added)
	// await Promise.all(config.AUTO_CONVERT_FORMATS.map(([from, to]) => {
	//     if (from.find(fromExt => fromExt === format)) {
	//         console.log('Converting', file.name, 'to', to);
	//         return Promise.all(to.map(async toExt => {
	//             // TODO: Check that format toExt for book doesn't already exist before converting
	//             const fnWithoutExt = path.basename(file.name).split('.').slice(0, -1).join('.');
	//             let filename, i = 0;
	//             do {
	//                 filename = `${fnWithoutExt}${i > 0 ? `-${i++}` : ''}.${toExt}`
	//             } while (i < 50 && fs.existsSync(filename));
	//             const target = path.join(config.BOOKS_DIR, filename);
	//             await Meta.convertBook(filepath, target);
	//             const md5 = md5File.sync(target);
	//             extraFiles.push({ md5, filename, format:toExt, meta, isConverted:true });
	//             fs.renameSync(target, path.resolve(config.BOOKS_DIR, md5));
	//         }));
	//     }
	// }));


	// TODO: Remove files with md5s that weren't saved in books...

	
});

module.exports = router;

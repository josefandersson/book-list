const router = require('express').Router();
const path = require('path');

const config = require('../config');
const fs = require('fs');

router.get('/', (_, res) => res.redirect('/'));

// Download file
router.get('/:filename', (req, res) => {
    // TODO: Check if book is hidden, etc?

    const filePath = path.resolve(config.BOOKS_DIR, encodeURIComponent(req.params.filename));

    if (fs.existsSync(filePath))
        res.sendFile(filePath);
    else
        res.send("file doesn't exist");
});

router.use((_, res) => res.redirect('/'));

module.exports = router;

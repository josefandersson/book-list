const router = require('express').Router();

const BookRepository = require('../src/BookRepository');

router.use((req, res, next) => {
	if (req.canEdit) {
		next();
	} else {
		res.json({ error: 'access denied' });
	}
});

router.get('/', (_, res) => {
	res.send(`
[GET] / - This page<br>
[POST] /books/merge ({ bookIds }) - Merge two or more books into one, first book id in list is kept`);
});

router.post('/books/merge', async (req, res) => {
	console.log(req.body.bookIds);
	if (!req.body.bookIds)
		return res.json({ error: 'missing bookIds' });

	await BookRepository.mergeBooks({ bookIds: req.body.bookIds });

	res.json({ nigger:true });
});

router.use((_, res) => res.json({ error: 'unknown endpoint' }));

module.exports = router;
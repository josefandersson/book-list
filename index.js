const fs = require('fs');
const path = require('path');
const express = require('express');
const expressip = require('express-ip');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

const config = require('./config');
const db = require('./src/Database');

if (!fs.existsSync(config.BOOKS_DIR))
    fs.mkdirSync(config.BOOKS_DIR);
if (!fs.existsSync('public/covers'))
    fs.mkdirSync('public/covers');

const app = express();

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressip().getIpInfoMiddleware);
app.use(express.static('public'));

app.use(require('./routes/auth'));
app.use('/api', require('./routes/api'));

app.use('/ul', fileUpload({
    useTempFiles: true,
    tempFileDir: config.TEMP_DIR,
    limits: { fileSize: config.MAX_FILE_SIZE }
}));
app.use('/ul', require('./routes/upload'));
app.get('/', require('./routes/index'));
app.use('/', require('./routes/download'));

db.runOnInit(() => app.listen(config.PORT || 8080, ).on('listening', () => console.log(`Started express server on http://localhost:${config.PORT || 8080}`)));
const mergeBooks = async bookIds => {
	const res = await fetch('/api/books/merge', {
		credentials: 'include',
		method: 'POST',
		headers: {
			'content-type': 'application/json'
		},
		body: JSON.stringify({ bookIds })
	});
	return res.json();
}


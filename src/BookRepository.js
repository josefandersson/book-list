const db = require('./Database');
const Util = require('./Util');

const getBooksByFilters = ({ sort = null, order = null, limit = 50, offset = 0 }) => {
	const _sort = Util.insideOrDefault(sort, Util.BOOK_FILTERS.sort);
	const _order = Util.insideOrDefault(order, Util.BOOK_FILTERS.order);

	const sql = `SELECT
	books.id,
	title,
	brief,
	authors.name AS author,
	published_date,
	cover_name,
	books.hidden,
	authors.hidden,
	books.created_at,
	(SELECT count(id)
		FROM files
		WHERE files.book_id = books.id) AS file_count,
	(SELECT count(id)
		FROM books_categories
		WHERE books_categories.book_id = books.id) AS category_count
FROM books
INNER JOIN authors ON books.author_id = authors.id
WHERE
	books.hidden = ?1
	AND authors.hidden = ?1
ORDER BY ${_sort} ${_order}
LIMIT ?2 OFFSET ?3`;

	return db.preparedAll(sql, 0, limit, offset);
};

const getCategoriesByFilter = () => db.preparedAll(
	`SELECT
	id,
	name,
	description,
	hidden,
	created_at
FROM categories
`);

const getCategoriesForBook = bookId => db.preparedAll(
	`SELECT
		books_categories.id,
		books_categories.created_at,
		categories.id,
		categories.name,
		categories.description,
		categories.hidden,
		categories.created_at
	FROM books_categories
	INNER JOIN categories
		ON books_categories.category_id = categories.id
	WHERE book_id = ?`, bookId);

const insertAuthor = async ({ name, hidden = null }) => {
	const params = [name,
		...hidden ? [hidden] : []];

	await db.preparedRun(`INSERT OR IGNORE INTO authors
	(name${hidden ? ', hidden' : ''})
VALUES
	(${[...params].fill('?').join(',')})`, params);

	const row = await db.preparedGet('SELECT id FROM authors WHERE name = ?', [name]);
	return row.id;
};

const insertBook = async ({ title, brief, authorId, publishedDate = null, coverFilename = null, hidden = null }) => {
	const params = [title, brief, authorId, publishedDate, coverFilename, ...hidden ? [hidden] : []];

	await db.preparedRun(`INSERT INTO books
	(title, brief, author_id, published_date, cover_name${hidden ? ', hidden' : ''})
VALUES
	(${[...params].fill('?').join(',')})`, params);

	const row = await db.preparedGet('SELECT id FROM books WHERE title = ? ORDER BY id DESC', title);
	return row.id;
};

const mergeBooks = async ({ bookIds }) => {
	// Move everything from bookIds[i>0] to bookIds[0]
	const list = [...bookIds].map((_, i) => `?${i+1}`).join(',');
	console.log(list)
	return db.preparedRun(`UPDATE books
SET author_id = ?1
WHERE author_id IN (${list});
DELETE FROM authors
WHERE id IN (${list});`, bookIds);
};

module.exports = {
	insertAuthor,
	insertBook,
	getBooksByFilters,
	getCategoriesByFilter,
	getCategoriesForBook,
	mergeBooks
};
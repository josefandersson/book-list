const sqlite3 = require('sqlite3');
const fs = require('fs');
const util = require('util');
const path = require('path');

const config = require('../config');

const RESET_DATABASE = 1
	* 0;

process.stdout.write('Opening database file...');
const db = new sqlite3.Database(config.BOOKS_DB, err => {
	if (err) {
		console.error('Could not open database file: %s', config.BOOKS_DB);
		throw err;
	} else {
		process.stdout.write('OK\n');
		initializeTables();
	}
});

const exec = util.promisify(db.exec.bind(db));
const all = util.promisify(db.all.bind(db));
const get = util.promisify(db.get.bind(db));
const prepare = db.prepare.bind(db);

const preparedStatement = (method, sql, ...params) => {
	return new Promise((resolve, reject) => {
		const stmt = db.prepare(sql, ...params);
		stmt[method]((err, res) => {
			if (err) reject(err);
			else resolve(res);
		});
	});
};
const preparedGet = (sql, ...params) => preparedStatement('get', sql, ...params);
const preparedAll = (sql, ...params) => preparedStatement('all', sql, ...params);
const preparedRun = (sql, ...params) => preparedStatement('run', sql, ...params);

const initializeTables = async () => {
	const init = async (name, sql) => {
		process.stdout.write(name + '...');
		try {
			await exec(sql);
			process.stdout.write('OK\n');
		} catch (err) {
			process.stdout.write('FAILED\n');
			throw err;
		}
	};

	RESET_DATABASE && await init('Deleting tables',
		`DROP TABLE IF EXISTS books_categories;
		DROP TABLE IF EXISTS categories;
		DROP TABLE IF EXISTS authors;
		DROP TABLE IF EXISTS books;
		DROP TABLE IF EXISTS files;
		DROP TABLE IF EXISTS file_formats;`);

	await init('Ensuring table `authors`',
		`CREATE TABLE IF NOT EXISTS authors (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name VARCHAR NOT NULL,
			hidden BIT NOT NULL DEFAULT 0,
			created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)`);

	await init('Ensuring table `books`',
		`CREATE TABLE IF NOT EXISTS books (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			title VARCHAR NOT NULL,
			brief VARCHAR,
			author_id INTEGER REFERENCES authors(id) ON DELETE SET NULL DEFAULT NULL,
			published_date DATE,
			cover_name VARCHAR,
			hidden BIT NOT NULL DEFAULT 0,
			created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)`);

	await init('Ensuring table `categories`',
		`CREATE TABLE IF NOT EXISTS categories (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name VARCHAR NOT NULL,
			description VARCHAR,
			hidden BIT NOT NULL DEFAULT 0,
			created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)`);

	await init('Ensuring table `books_categories`',
		`CREATE TABLE IF NOT EXISTS books_categories (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			book_id INTEGER REFERENCES books(id) ON DELETE CASCADE,
			category_id INTEGER REFERENCES categories(id) ON DELETE CASCADE,
			created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)`);

	await init('Ensuring table `file_formats`',
		`CREATE TABLE IF NOT EXISTS file_formats (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			extension VARCHAR UNIQUE,
			mimetype VARCHAR,
			hidden BIT NOT NULL DEFAULT 0,
			kobo BIT NOT NULL DEFAULT 1,
			kindle BIT NOT NULL DEFAULT 0
)`);

	await init('Ensuring table `files`',
		`CREATE TABLE IF NOT EXISTS files (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name VARCHAR UNIQUE,
			size INTEGER NOT NULL,
			md5 VARCHAR UNIQUE,
			original_name VARCHAR,
			cover_name VARCHAR,
			metadata VARCHAR,
			book_id INTEGER REFERENCES books(id) ON DELETE SET NULL,
			file_format_id INTEGER REFERENCES file_formats(id),
			created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)`);

	await init('Ensuring table `file_blobs`',
		`CREATE TABLE IF NOT EXISTS file_blobs (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			file_id INTEGER REFERENCES files(id) ON DELETE CASCADE,
			blob BLOB,
			cover_blob BLOB,
			created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)`);

	if (config.BACKUP_BLOBS_IN_DB) {
		process.stdout.write('Syncing file blobs from db...');
		try {
			const files = await preparedAll(
				`SELECT
					file_blobs.blob,
					file_blobs.cover_blob,
					files.name,
					files.cover_name
				FROM file_blobs
				INNER JOIN files
					ON file_blobs.file_id = files.id`);
			await Promise.all(files.map(async fileBlob => {
				const bookPath = path.resolve(config.BOOKS_DIR, fileBlob.name);
				const coverPath = path.resolve('public/covers', fileBlob.cover_name);

				if (!fs.existsSync(bookPath))
					fs.writeFileSync(bookPath, fileBlob.blob);

				if (!fs.existsSync(coverPath))
					fs.writeFileSync(coverPath, fileBlob.cover_blob);
			}));
		} catch (err) {
			process.stdout.write('FAILED\n');
			throw err;
		}
		process.stdout.write('OK\n');

		process.stdout.write('Syncing file blobs to db...');
		try {
			const files = await preparedAll(
				`SELECT
					files.id,
					files.name,
					files.cover_name
				FROM files
				LEFT JOIN file_blobs
					ON files.id = file_blobs.file_id
				WHERE file_blobs.id IS NULL`);
			await Promise.all(files.map(async file => {
				const bookPath = path.resolve(config.BOOKS_DIR, file.name);
				const coverPath = path.resolve('public/covers', file.cover_name);

				const blob = bookPath ? fs.readFileSync(bookPath) : null;
				const coverBlob = coverPath ? fs.readFileSync(coverPath) : null;

				await preparedRun('INSERT INTO file_blobs (file_id, blob, cover_blob) VALUES (?, ?, ?)', [file.id, blob, coverBlob]);
			}));
		} catch (err) {
			process.stdout.write('FAILED\n');
			throw err;
		}
		process.stdout.write('OK\n');

		onInit();
	}
};

let onInit;
const runOnInit = (func) => {
	onInit = func;
};

module.exports = {
	all,
	db,
	exec,
	get,
	prepare,
	preparedGet,
	preparedAll,
	preparedRun,
	runOnInit
};
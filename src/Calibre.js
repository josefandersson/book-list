const { exec } = require('child_process');
const path = require('path');

/**
 * Use ebook-meta to get a book files metadata
 * @param {String} bookPath Full or relative path to book
 * @param {String|null} coverDirectory Directory to export cover image to, also needs coverFilename
 * @param {String|null} coverFilename Name of exported cover file
 * @returns {Object} Book metadata
 */
const getBookMeta = (bookPath, coverDirectory = null, coverFilename = null) => {
    return new Promise((resolve, reject) => {
        const cmd = 'ebook-meta '
            + (coverDirectory && coverFilename
                ? `--get-cover="${path.resolve(coverDirectory, coverFilename)}" `
                : '')
            + `"${bookPath}" `;
        exec(cmd , (err, stdout) => {
            if (err) reject(err);
            else {
                const meta = {
                    coverFilename: stdout.indexOf('Cover saved to') > -1
                        ? coverFilename
                        : null
                };
                stdout
                    .split('\n')
                    .map(ln => ln.split(':'))
                    .filter(x => x.length >= 2)
                    .forEach(([key, ...val]) => {
                        const valStr = val.join(':').trim();
                        if (key.length && valStr.length)
                            meta[key.trim().toLowerCase().replace(/[^a-z0-9]/g, '')] = valStr;
                });
                resolve(meta);
            }
        });
    });
};

/**
 * Convert book at source to book at target with other extension
 * @param {String} source 
 * @param {String} target 
 */
const convertBook = (source, target) => {
    return new Promise((resolve, reject) => {
        exec(`ebook-convert "${source}" "${target}"`, err => {
            if (err) reject(err);
            else resolve();
        });
    });
};

module.exports = {
    getBookMeta,
    convertBook
};
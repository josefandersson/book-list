const db = require('./Database');

const insertFileFormat = async ({ extension, mimetype = null, hidden = null, kobo = null, kindle = null }) => {
	const params = [extension, mimetype,
		...hidden ? [hidden] : [],
		...kobo ? [kobo] : [],
		...kindle ? [kindle] : []];

	await db.preparedRun(`INSERT OR IGNORE INTO file_formats
(extension, mimetype${hidden ? ', hidden' : ''}${kobo ? ', kobo' : ''}${kindle ? ', kindle' : ''})
VALUES
(${[...params].fill('?').join(',')})`, params);

	const row = await db.preparedGet('SELECT id FROM file_formats WHERE extension = ?', [extension]);
	return row.id;
};

const insertFile = async ({ bookId, fileFormatId, name, size, md5, originalName = null, coverFilename = null, metadata = null, blob = null, cover_blob = null }) => {
	await db.preparedRun(`INSERT INTO files
	(name, size, md5, original_name, cover_name, metadata, book_id, file_format_id)
VALUES
	(?, ?, ?, ?, ?, ?, ?, ?)`, [name, size, md5, originalName, coverFilename, metadata, bookId, fileFormatId]);
	
	const row = await (db.preparedGet('SELECT id FROM files WHERE md5 = ?', md5));
	const fileId = row.id;

	if (blob || cover_blob)
		await db.preparedRun(`INSERT INTO file_blobs (file_id, blob, cover_blob) VALUES (?, ?, ?)`, [fileId, blob, cover_blob]);

	return fileId;
};

const md5Exists = async md5 => (await db.preparedGet('SELECT id FROM files WHERE md5 = ?', md5)) != null;

const getFilesForBook = (bookId, kobo=null, kindle=null) => db.preparedAll(`SELECT
	files.id,
	files.name,
	files.size,
	files.md5,
	files.original_name,
	files.metadata,
	files.created_at,
	file_formats.extension,
	file_formats.hidden,
	file_formats.kobo,
	file_formats.kindle
FROM files
INNER JOIN file_formats
	ON files.file_format_id = file_formats.id
WHERE
	book_id = ?
	${kobo === true ? 'AND kobo = 1' : ''}
	${kindle === true ? 'AND kindle = 1' : ''}`, bookId);

module.exports = {
	getFilesForBook,
	insertFile,
	insertFileFormat,
	md5Exists,
};
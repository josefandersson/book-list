const fs = require('fs');
const path = require('path');

const BOOK_FILTERS = {
	sort: ['books.created_at', 'books.title', 'authors.name', 'books.published_date'],
	order: ['desc', 'asc'],
};
const BOOK_FILTERS_MAP = {
	latest: 'books.created',
	title: 'books.title',
	author: 'authors.name',
	published: 'books.published_date'
};

const nextFilename = (directory, filename, extension) => {
	let newPathname, newFilename;
	let i = 0;
	do {
		newFilename = i === 0
			? `${filename}.${extension}`
			: `${filename}_${i}.${extension}`;
		newPathname = path.resolve(directory, newFilename);
		i++;
	} while (fs.existsSync(newPathname));
	return newFilename;
};

const insideOrDefault = (value, array, defaultValue = null) => array.indexOf(value) > -1 ? value : defaultValue ?? array[0];

module.exports = {
	BOOK_FILTERS,
	BOOK_FILTERS_MAP,
	insideOrDefault,
	nextFilename,
};
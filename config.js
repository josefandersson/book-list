module.exports = {
    TEMP_DIR: '/tmp/books',
    BOOKS_DIR: 'books',
    BOOKS_DB: 'books.sqlite3',
    MAX_FILE_SIZE: 1024 ** 3, // 1 GB
    PORT: 8103,

    BACKUP_BLOBS_IN_DB: true,

    ALLOWED_MIMES: [
        'application/vnd.amazon.ebook', // azw
        'application/epub+zip', // epub
        'application/x-mobipocket-ebook', // mobi
        'application/pdf', // pdf
        'image/jpeg', // jpg, jpeg
        'image/png', // png
        'application/rtf', // rtf
        'text/plain', // txt
    ],

    AUTO_CONVERT_MIMES: [
        [['application/epub+zip'], ['application/x-mobipocket-ebook']],
        [['application/x-mobipocket-ebook'], ['application/epub+zip']],
    ],
};
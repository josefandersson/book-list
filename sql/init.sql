-- RESET DATABASE
DROP TABLE IF EXISTS books_categories;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS files;
DROP TABLE IF EXISTS file_formats;

-- CREATE TABLES
CREATE TABLE IF NOT EXISTS authors (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name VARCHAR NOT NULL,
	hidden BIT NOT NULL DEFAULT 0,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS books (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	title VARCHAR NOT NULL,
	brief VARCHAR,
	author_id INTEGER REFERENCES authors(id) ON DELETE
	SET
		NULL DEFAULT NULL,
		published_date DATE,
		cover_filename VARCHAR,
		hidden BIT NOT NULL DEFAULT 0,
		metadata VARCHAR,
		created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS categories (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name VARCHAR NOT NULL,
	description VARCHAR,
	hidden BIT NOT NULL DEFAULT 0,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS books_categories (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	book_id INTEGER REFERENCES books(id) ON DELETE CASCADE,
	category_id INTEGER REFERENCES categories(id) ON DELETE CASCADE,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS file_formats (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	extension VARCHAR UNIQUE,
	mime VARCHAR,
	supperted_kobo BIT NOT NULL DEFAULT 1,
	supperted_kindle BIT NOT NULL DEFAULT 0,
	supperted_other BIT NOT NULL DEFAULT 1
);

CREATE TABLE IF NOT EXISTS files (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name VARCHAR UNIQUE,
	size INTEGER NOT NULL,
	is_original BIT NOT NULL DEFAULT 1,
	original_name VARCHAR,
	book_id INTEGER REFERENCES books(id) ON DELETE
	SET
		NULL,
		file_format_id INTEGER REFERENCES file_formats(id),
		created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

-- CREATE INDICES
CREATE INDEX authors_name ON authors (name);
CREATE INDEX books_title ON books (title);

-- INSERT TEST DATA
INSERT INTO
	authors (name)
VALUES
	("Test Guy");

INSERT INTO
	books (title, author_id)
SELECT
	"Test Book 1",
	last_insert_rowid()
FROM
	authors;

INSERT INTO books (title, author_id) SELECT
	"Test Book 2",
	last_insert_rowid()
FROM authors;

INSERT INTO categories (name) VALUES
	("Test Category 1"),
	("Test Category 2");

INSERT INTO books_categories (book_id, category_id) VALUES
	(1, 1),
	(1, 2),
	(2, 2);